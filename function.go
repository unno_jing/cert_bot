package handler

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"runtime/debug"
	"strconv"
	"strings"

	"bitbucket.org/unno_jing/cert_bot/commands"
	"bitbucket.org/unno_jing/cert_bot/manage"

	"github.com/urfave/cli/v2"
)

// @jcert_bot

type Chat struct {
	Id   int    `json:"id"`
	Type string `json:"type"`
}

type Message struct {
	Id   int    `json:"message_id"`
	Text string `json:"text"`
	Chat Chat   `json:"chat"`
	From From   `json:"from"`
}

type From struct {
	Id       int    `json:"id"`
	IsBot    bool   `json:"is_bot"`
	Username string `json:"username"`
}

type Update struct {
	Updated int     `json:"updated_id"`
	Message Message `json:"message"`
}

type Response struct {
	OK          bool    `json:"ok"`
	Result      Message `json:"result"`
	ErrCode     int     `json:"error_code"`
	Description string  `json:"description"`
}

// a hard-code structure for the grafana webhook

type EvalMatch struct {
	Metric string  `json:"metric"`
	Value  float64 `json:"value"`
}

// this structure is deprecated because it's incompatible for a newer version grafana
// type GrafanaWebhook struct {
// 	Title       string      `json:"title"`
// 	State       string      `json:"state"`
// 	RuleURL     string      `json:"ruleUrl"`
// 	RuleName    string      `json:"ruleName"`
// 	Message     string      `json:"message"`
// 	ImageURL    string      `json:"imageUrl"`
// 	EvalMatches []EvalMatch `json:"evalMatches"`
// }

// {
//     "title": "[Alerting] Test notification",
//     "tags": {
//     },
//     "state": "alerting",
//     "ruleUrl": "https://monitor.grafana.h9339.com/",
//     "ruleName": "Test notification",
//     "ruleId": 0,
//     "panelId": 1,
//     "orgId": 0,
//     "message": "Someone is testing the alert notification within Grafana.",
//     "imageUrl": "https://grafana.com/assets/img/blog/mixed_styles.png",
//     "evalMatches": [
//         {
//             "tags": null,
//             "metric": "High value",
//             "value": 100
//         },
//         {
//             "tags": null,
//             "metric": "Higher Value",
//             "value": 200
//         }
//     ],
//     "dashboardId": 1
// }

type GrafanaWebhook struct {
	Title             string           `json:"title"`
	State             string           `json:"state"`
	Alerts            []Alert          `json:"alerts"`
	CommonAnnotations CommonAnnotation `json:"commonAnnotations"`
}

type CommonAnnotation struct {
	Summary string `json:"summary"`
}

type Alert struct {
	ImageURL    string `json:"imageURL"`
	RuleURL     string `json:"panelURL"`
	ValueString string `json:"valueString"`
}

// The new format
// {
//   "receiver": "my-telegram",
//   "status": "firing",
//   "alerts": [
//     {
//       "status": "firing",
//       "labels": {
//         "alertname": "CPU-api",
//         "notification": "telegram"
//       },
//       "annotations": {
//         "summary": "api cpu 需要注意"
//       },
//       "startsAt": "2022-08-23T10:08:07.707257776Z",
//       "endsAt": "0001-01-01T00:00:00Z",
//       "generatorURL": "https://monitor.grafana.clzud.live/alerting/grafana/tq590CiVz/view",
//       "fingerprint": "b4572834453a58f0",
//       "silenceURL": "https://monitor.grafana.clzud.live/alerting/silence/new?alertmanager=grafana&matcher=alertname%3DCPU-api&matcher=notification%3Dtelegram",
//       "dashboardURL": "https://monitor.grafana.clzud.live/d/kcTW2jm4k",
//       "panelURL": "https://monitor.grafana.clzud.live/d/kcTW2jm4k?viewPanel=9",
//       "valueString": "[ var='B0' metric='ghost-78567ffd56-7ps7b' labels={pod=ghost-78567ffd56-7ps7b} value=0.27457304663672766 ], [ var='B1' metric='ghost-78567ffd56-8pzgq' labels={pod=ghost-78567ffd56-8pzgq} value=0.25986841973506186 ], [ var='B2' metric='ghost-78567ffd56-gkwb6' labels={pod=ghost-78567ffd56-gkwb6} value=0.23018106843145453 ], [ var='B3' metric='ghost-78567ffd56-nfjp4' labels={pod=ghost-78567ffd56-nfjp4} value=0.2635864958046286 ], [ var='B4' metric='ghost-78567ffd56-tb72n' labels={pod=ghost-78567ffd56-tb72n} value=0.29957900127242937 ], [ var='B5' metric='ghost-78567ffd56-tmtw8' labels={pod=ghost-78567ffd56-tmtw8} value=0.3169962672428377 ]",
//       "imageURL": "https://monitor.grafana.clzud.live/public/img/attachments/r6Hl5tCdYP5jIoAKf99V.png"
//     }
//   ],
//   "groupLabels": {},
//   "commonLabels": {
//     "alertname": "CPU-api",
//     "notification": "telegram"
//   },
//   "commonAnnotations": {
//     "summary": "api cpu 需要注意"
//   },
//   "externalURL": "https://monitor.grafana.clzud.live/",
//   "version": "1",
//   "groupKey": "{}/{notification=\"telegram\"}:{}",
//   "truncatedAlerts": 0,
//   "orgId": 1,
//   "title": "[FIRING:1] (CPU-api telegram)",
//   "state": "alerting",
//   "message": "**Firing**\n\nValue: [ var='B0' metric='ghost-78567ffd56-7ps7b' labels={pod=ghost-78567ffd56-7ps7b} value=0.27457304663672766 ], [ var='B1' metric='ghost-78567ffd56-8pzgq' labels={pod=ghost-78567ffd56-8pzgq} value=0.25986841973506186 ], [ var='B2' metric='ghost-78567ffd56-gkwb6' labels={pod=ghost-78567ffd56-gkwb6} value=0.23018106843145453 ], [ var='B3' metric='ghost-78567ffd56-nfjp4' labels={pod=ghost-78567ffd56-nfjp4} value=0.2635864958046286 ], [ var='B4' metric='ghost-78567ffd56-tb72n' labels={pod=ghost-78567ffd56-tb72n} value=0.29957900127242937 ], [ var='B5' metric='ghost-78567ffd56-tmtw8' labels={pod=ghost-78567ffd56-tmtw8} value=0.3169962672428377 ]\nLabels:\n - alertname = CPU-api\n - notification = telegram\nAnnotations:\n - summary = api cpu 需要注意\nSource: https://monitor.grafana.clzud.live/alerting/grafana/tq590CiVz/view\nSilence: https://monitor.grafana.clzud.live/alerting/silence/new?alertmanager=grafana&matcher=alertname%3DCPU-api&matcher=notification%3Dtelegram\nDashboard: https://monitor.grafana.clzud.live/d/kcTW2jm4k\nPanel: https://monitor.grafana.clzud.live/d/kcTW2jm4k?viewPanel=9\n"
// }

func parseMetrics(content string) string {
	result := ""

	re := regexp.MustCompile(`(metric|value)+='?[a-zA-Z0-9-.]+'?`)
	matches := re.FindAllString(content, -1)
	for i := 0; i < len(matches); i += 2 {
		key := strings.Split(matches[i], "=")[1]
		value := strings.Split(matches[i+1], "=")[1]
		result = result + fmt.Sprintf("%s: %s\n", key, value)
	}

	return result
}

func generateAlertMessage(content *GrafanaWebhook) (string, error) {
	metrics := ""
	alert := content.Alerts[0]

	if alert.ValueString != "" {
		metrics = parseMetrics(alert.ValueString)
	}

	// format
	// [state] title
	// Rule: rulename
	// Message: message
	// URL: ruleURL
	// Metrics:
	//  blabla
	tmpl := "%s\n" +
		"Message: %s\n" +
		"URL: %s\n\n"

	if metrics != "" {
		tmpl = tmpl + "Metrics: \n" + metrics
	}

	result := fmt.Sprintf(tmpl, content.Title, content.CommonAnnotations.Summary, alert.RuleURL)

	return result, nil
}

func isFromGrafana(r *http.Request) bool {
	source := r.URL.Query().Get("source")
	// NOTE: there is no way to judge the request is from grafana
	// the only way is appending the custom query params.
	// example url
	// https://asia-east2-staging-211402.cloudfunctions.net/cert?source=grafana&env=staging
	return source == "grafana"
}

func getGrafanaTargetChatID(r *http.Request) (int, error) {

	env := r.URL.Query().Get("env")
	if env == "staging" {
		// dynamically set the chat id
	}

	chatID, err := strconv.Atoi(getEnv("T_ALERT_CHAT_ID", "-533036721"))
	if err != nil {
		return 0, err
	}

	return chatID, nil
}

func handleRequestFromGrafana(w http.ResponseWriter, content []byte, chatID int) error {

	var hookContent GrafanaWebhook

	log.Printf("incoming content: %s\n", string(content))

	if err := json.Unmarshal(content, &hookContent); err != nil {
		log.Printf("json unmarshal error: %s\n", err.Error())
		return err
	}

	// start to generate the alert notification sent to telegram further
	resultMsg, err := generateAlertMessage(&hookContent)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return nil
	}

	if len(hookContent.Alerts) > 0 {

		alert := hookContent.Alerts[0]

		if alert.ImageURL != "" {
			res, err := sendPhoto(chatID, alert.ImageURL)
			if err != nil {
				w.WriteHeader(400)
				w.Write([]byte(err.Error()))
				return nil
			}
			log.Printf("response for send photo: %s\n", res)
		}
	}

	res, err := sendMsg(chatID, resultMsg)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return nil
	}

	log.Printf("response: %s\n", res)
	return nil
}

func handleNormalUpdate(w http.ResponseWriter, content []byte) {

	var update Update

	log.Printf("incoming content: %s\n", string(content))

	if err := json.Unmarshal(content, &update); err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}

	// process the incomming message
	log.Printf("received: %s", update.Message.Text)

	if !isAllowdToPerformCommand(update.Message.From.Username) {

		res, err := sendMsg(update.Message.Chat.Id, "You are not allowed to send commands")
		if err != nil {
			log.Printf("error: %s\n", err.Error())
		}

		log.Printf("response: %s\n", res)
		return

	}

	commands := parseCommandMessage(update.Message.Text)
	log.Printf("command: %v to be performed \n", commands)
	resultMsg := PerformCommands(commands)

	// send feedback to the client
	res, err := sendMsg(update.Message.Chat.Id, resultMsg)
	if err != nil {
		_, err := sendMsg(update.Message.Chat.Id, err.Error())
		if err != nil {
			log.Printf("send error message fail: %s\n", err.Error())
		}
	}

	log.Printf("response: %s\n", res)

}

func getEnv(key string, value string) string {
	v := os.Getenv(key)
	if v == "" {
		v = value
	}
	return v
}

func setEnv(key, value string) error {
	return os.Setenv(key, value)
}

var token = getEnv("T_TOKEN", "1445677352:AAGBMKs203DArfekBdey8ghqSmMWiU3a3p4")
var allow_users = getEnv("T_USERS", "jingsin")

var apiEndpoint = "https://api.telegram.org/bot" + token
var sendMsgAPIEndpoint = apiEndpoint + "/sendMessage"
var sendPhotoAPIEndpoint = apiEndpoint + "/sendPhoto"

func isAllowdToPerformCommand(user string) bool {
	users := strings.Split(allow_users, ",")

	for _, u := range users {
		if u == user {
			return true
		}
	}
	return false
}

func sendPhoto(chatId int, photoURL string) (string, error) {
	response, err := http.PostForm(sendPhotoAPIEndpoint, url.Values{
		"chat_id": {strconv.Itoa(chatId)},
		"photo":   {photoURL},
	})

	if err != nil {
		return "", err
	}

	defer response.Body.Close()

	res := Response{}

	var bodyBytes, errRead = ioutil.ReadAll(response.Body)
	if errRead != nil {
		return "", err
	}

	if err := json.Unmarshal(bodyBytes, &res); err != nil {
		return string(bodyBytes), err
	}

	if !res.OK {
		return "", errors.New(res.Description)
	}

	return string(bodyBytes), nil
}

func sendMsg(chatId int, text string) (string, error) {

	response, err := http.PostForm(sendMsgAPIEndpoint, url.Values{
		"chat_id": {strconv.Itoa(chatId)},
		"text":    {text},
	})

	if err != nil {
		return "", err
	}

	defer response.Body.Close()

	res := Response{}

	var bodyBytes, errRead = ioutil.ReadAll(response.Body)
	if errRead != nil {
		return "", err
	}

	if err := json.Unmarshal(bodyBytes, &res); err != nil {
		return string(bodyBytes), err
	}

	if !res.OK {
		return "", errors.New(res.Description)
	}

	return string(bodyBytes), nil
}

func PerformCommands(args []string) string {
	app := cli.NewApp()
	app.EnableBashCompletion = true
	app.ExitErrHandler = func(context *cli.Context, err error) {
	}

	var buf bytes.Buffer
	app.Writer = &buf
	app.Authors = []*cli.Author{
		{
			Name:  "Jing",
			Email: "secret@gmail.com",
		},
	}

	// register cert relevant commands
	certCmd := manage.RegisterCommand(app, commands.RegisterGCPCertCmd)
	manage.RegisterSubCommand(app, certCmd, commands.RegisterGCPCertCreateCmd)
	manage.RegisterSubCommand(app, certCmd, commands.RegisterGCPCertDeletecmd)
	manage.RegisterSubCommand(app, certCmd, commands.RegisterGCPCertMergecmd)
	manage.RegisterSubCommand(app, certCmd, commands.RegisterGCPCertListcmd)

	// register httpstargerproxy relevant commands
	targetCmd := manage.RegisterCommand(app, commands.RegisterGCPHttpTargetProxy)
	manage.RegisterSubCommand(app, targetCmd, commands.RegisterGCPHttpTargetProxyListCmd)
	manage.RegisterSubCommand(app, targetCmd, commands.RegisterGCPHttpTargetProxyAppendCertCmd)
	manage.RegisterSubCommand(app, targetCmd, commands.RegisterGCPHttpTargetProxyUpdateCertCmd)
	manage.RegisterSubCommand(app, targetCmd, commands.RegisterGCPHttpTargetProxyRemoveCertCmd)

	// register ssl relevant commands
	sslCmd := manage.RegisterCommand(app, commands.RegisterSSL)
	manage.RegisterSubCommand(app, sslCmd, commands.RegisterSSLCheckCmd)

	// register whois relevant commands
	manage.RegisterCommand(app, commands.RegisterWhoisCmd)

	// register echo command
	manage.RegisterCommand(app, commands.RegisterEchoCmd)

	// register ansible relevant commands
	ansibleCmd := manage.RegisterCommand(app, commands.RegisterAnsibleCmd)
	manage.RegisterSubCommand(app, ansibleCmd, commands.RegisterAnsibleAddDomainsAndDeploy)
	manage.RegisterSubCommand(app, ansibleCmd, commands.RegisterAnsibleMaintainFrontendCmd)
	manage.RegisterSubCommand(app, ansibleCmd, commands.RegisterAnsibleUnMaintainFrontendCmd)
	manage.RegisterSubCommand(app, ansibleCmd, commands.RegisterAnsibleTriggerCmd)

	err := app.Run(args)
	if err != nil {
		log.Print(err)
		return err.Error()
	}

	return buf.String()
}

func parseCommandMessage(message string) []string {
	tokens := strings.Split(message, " ")
	commands := []string{os.Args[0]}

	if strings.HasPrefix(tokens[0], "/") {
		commands = append(commands, tokens[1:]...)
	} else {
		commands = append(commands, tokens...)
	}

	// to finalize the command tokens, sometime we will encounter some tokens
	// should be grouped into one token. ex "deploy staging frontend"
	// it should not be treated as multi tokens like ="deploy= =staging= =frontend"=
	refinedCommands := []string{}
	part := []string{}

	for _, t := range commands {
		if strings.HasPrefix(t, "\"") || strings.HasPrefix(t, "“") {
			if strings.HasSuffix(t, "\"") || strings.HasSuffix(t, "”") {
				part = append(part, t[1:len(t)-1])
				refinedCommands = append(refinedCommands, strings.Join(part, " "))
				part = []string{}
			} else {
				part = append(part, t[1:])
			}

		} else if strings.HasSuffix(t, "\"") || strings.HasSuffix(t, "”") {
			part = append(part, t[:len(t)-1])
			refinedCommands = append(refinedCommands, strings.Join(part, " "))
			part = []string{}
		} else if len(part) != 0 {
			part = append(part, t)
		} else if len(part) == 0 {
			refinedCommands = append(refinedCommands, t)
		}
	}

	return refinedCommands
}

// HandleUpdate handles the incoming =Update=
// from the Telegram webhook
func HandleUpdate(w http.ResponseWriter, r *http.Request) {

	defer func() {
		if r := recover(); r != nil {
			log.Printf("stack from panic: %s\n", debug.Stack())
		}
	}()

	content, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}
	defer r.Body.Close()

	log.Printf("host: %+v, %s, %s", r.Header, r.Host, r.URL.Host)

	if isFromGrafana(r) {
		chatID, err := getGrafanaTargetChatID(r)
		if err != nil {
			w.WriteHeader(400)
			w.Write([]byte(err.Error()))
			return
		}

		err = handleRequestFromGrafana(w, content, chatID)
		if err != nil {
			w.WriteHeader(400)
			w.Write([]byte(err.Error()))
			return
		}
		return
	}

	handleNormalUpdate(w, content)
}
