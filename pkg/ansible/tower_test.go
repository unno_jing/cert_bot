package ansible

import (
	"fmt"
	"testing"
)

func TestGetJobTemplateList(t *testing.T) {

	res, err := GetJobTemplateList()
	if err != nil {
		t.Error(err)
	}

	fmt.Println(res)

}

func TestGetInventoryList(t *testing.T) {
	res, err := GetInventoryList()
	if err != nil {
		t.Error(err)
	}

	fmt.Println(res)
}
