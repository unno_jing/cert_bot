package ansible

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"gopkg.in/square/go-jose.v2/json"
)

func makeRequest(path string, method string, payload interface{}) ([]byte, error) {

	dataByte, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, path, bytes.NewBuffer(dataByte))
	if err != nil {
		return nil, err
	}

	v := os.Getenv("T_ANSIBLE_TOKEN")
	if v == "" {
		// set a default token if env is not configured.
		v = "V0qb5J4wkezDlI1kfLrEQkFP8L2cOy"
	}

	req.Header.Set("Authorization", "Bearer "+v)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	return resBody, nil
}

var apiHost = "https://staging-awx.h9339.com"

type Inventory struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type Summary struct {
	Inventory Inventory `json:"inventory"`
}

type JobTemplate struct {
	ID            int     `json:"id"`
	Name          string  `json:"name"`
	ExtraVars     string  `json:"extra_vars"`
	Status        string  `json:"status"`
	SummaryFields Summary `json:"summary_fields"`
}

type InventoryListResponse struct {
	Results []Inventory `json:"results"`
}

type JobTemplateListResponse struct {
	Results []JobTemplate `json:"results"`
}

// GetJobTemplateList retrieves the list of job templates on the ansible tower
func GetJobTemplateList() ([]JobTemplate, error) {
	apiPath := "/api/v2/job_templates/"

	res, err := makeRequest(apiHost+apiPath, "GET", nil)
	if err != nil {
		return nil, err
	}

	list := &JobTemplateListResponse{}
	err = json.Unmarshal(res, &list)
	if err != nil {
		return nil, err
	}

	return list.Results, nil
}

// LaunchJobTemplatePayload is the palyload
// of launching ansible job template
type LaunchJobTemplatePayload struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	ExtraVars string `json:"extra_vars"`
}

func getJobTemplateIDByName(name string) (*LaunchJobTemplatePayload, error) {
	// unify the key is lower case
	name = strings.ToLower(name)

	nameToIDMap := map[string]*LaunchJobTemplatePayload{}

	list, err := GetJobTemplateList()
	if err != nil {
		return nil, err
	}

	for _, obj := range list {
		nameToIDMap[strings.ToLower(obj.Name)] = &LaunchJobTemplatePayload{
			ID:        obj.ID,
			Name:      obj.Name,
			ExtraVars: obj.ExtraVars,
		}
	}

	if _, ok := nameToIDMap[name]; !ok {
		return nil, fmt.Errorf("no such job template: %s", name)
	}

	return nameToIDMap[name], nil
}

func GetInventoryList() ([]Inventory, error) {
	apiPath := "/api/v2/inventories/"

	res, err := makeRequest(apiHost+apiPath, "GET", nil)
	if err != nil {
		return nil, err
	}

	list := &InventoryListResponse{}
	err = json.Unmarshal(res, &list)
	if err != nil {
		return nil, err
	}
	return list.Results, nil
}

func GetInventoryIDByName(name string) (int, error) {
	name = strings.ToLower(name)

	nameToID := map[string]int{}

	list, err := GetInventoryList()
	if err != nil {
		return 0, err
	}

	for _, obj := range list {
		nameToID[strings.ToLower(obj.Name)] = obj.ID
	}

	if _, ok := nameToID[name]; !ok {
		return 0, fmt.Errorf("no such inventory: %s", name)
	}

	return nameToID[name], nil
}

// TriggerJobPayLoad is the payload for triggering the job
type TriggerJobPayLoad struct {
	ExtraVars string `json:"extra_vars,omitempty"`
	Inventory int    `json:"inventory,omitempty"`
}

type Processor func(*TriggerJobPayLoad) error

type JobResponse struct {
	ID   int    `json:"id"`
	URL  string `json:"url"`
	Name string `json:"name"`
}

func EditJobTemplate(name string, proc Processor) (*JobResponse, error) {
	apiPathTmpl := "/api/v2/job_templates/%d/"
	launchPayload, err := getJobTemplateIDByName(name)
	if err != nil {
		return nil, err
	}

	apiPath := fmt.Sprintf(apiPathTmpl, launchPayload.ID)
	payload := &TriggerJobPayLoad{
		ExtraVars: launchPayload.ExtraVars,
	}

	if proc != nil {
		proc(payload)
	}

	resByte, err := makeRequest(apiHost+apiPath, "PATCH", payload)
	if err != nil {
		return nil, err
	}

	res := &JobResponse{}

	err = json.Unmarshal(resByte, &res)
	if err != nil {
		return nil, err
	}

	return res, nil
}

// TriggerJobTemplate launches the specify job template
func TriggerJobTemplate(name string, proc Processor) (*JobResponse, error) {

	apiPathTmpl := "/api/v2/job_templates/%d/launch/"
	launchPayload, err := getJobTemplateIDByName(name)
	if err != nil {
		return nil, err
	}

	apiPath := fmt.Sprintf(apiPathTmpl, launchPayload.ID)
	payload := &TriggerJobPayLoad{
		ExtraVars: launchPayload.ExtraVars,
	}

	// Provide a way to change the payload
	if proc != nil {
		err = proc(payload)
		if err != nil {
			return nil, err
		}
	}

	resByte, err := makeRequest(apiHost+apiPath, "POST", payload)
	if err != nil {
		return nil, err
	}

	res := &JobResponse{}

	err = json.Unmarshal(resByte, &res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
