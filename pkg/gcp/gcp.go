package gcp

import (
	"context"
	"fmt"
	"io"
	"regexp"
	"strings"

	"google.golang.org/api/compute/v1"
)

// Problem! I need to delete the old certificates but how do I know which one to delete
// Maybe I can just delete it and create it quickly?
// The ssl has cache.
// https://cloud.google.com/sdk/gcloud/reference/compute/target-https-proxies/update

// NewAPIClient news a api client for gcp
func NewAPIClient(ctx context.Context, project string) (*APIClient, error) {
	computeService, err := compute.NewService(ctx)
	if err != nil {
		return nil, err
	}

	return &APIClient{
		ctx:            ctx,
		computeService: computeService,
		project:        project,
	}, nil
}

// APIClient is a api client for gcp
type APIClient struct {
	ctx            context.Context
	computeService *compute.Service
	project        string
}

func CreateForwardingMapOfIPToTarget(res []TargetProxyResponse) map[string]string {
	result := map[string]string{}
	for _, r := range res {
		result[r.IP] = r.Target
	}
	return result
}

func CreateCertsMapOfNameToURI(certsList []*compute.SslCertificate) map[string]string {
	result := map[string]string{}

	for _, cert := range certsList {
		result[cert.Name] = cert.SelfLink
	}

	return result
}

func formatDomainStatus(ds map[string]string) string {
	colorReset := "\033[0m"
	colorRed := "\033[31m"
	colorGreen := "\033[32m"

	domains := []string{}

	for domain, status := range ds {
		if status == "ACTIVE" {
			domains = append(domains, domain, colorGreen, "v", colorReset)
		} else {
			domains = append(domains, domain, colorRed, "x", colorReset)
		}
	}

	return strings.Join(domains, " ")
}

func PrettyPrintCerts(writer io.Writer, certslist []*compute.SslCertificate) {

	formatString := "%-30v %-20v\n"

	fmt.Printf(formatString, "Name", "Domains")
	for _, cert := range certslist {
		if cert.Managed != nil {
			fmt.Fprintf(writer, formatString, cert.Name, formatDomainStatus(cert.Managed.DomainStatus))
		} else {
			fmt.Fprintf(writer, formatString, cert.Name, cert.SubjectAlternativeNames)
		}
	}
}

func getDomainsOfGCPCerts(cert *compute.SslCertificate) []string {
	if cert.Managed != nil {
		return cert.Managed.Domains
	}

	return cert.SubjectAlternativeNames
}

// TODO: think a generic way to pretty print the content

func PrettyPrintTargetProxy(writer io.Writer, targetList []TargetProxyResponse) {
	formatString := "%-55v %-20v %-55v %-10v %-40v\n"
	fmt.Fprintf(writer, formatString, "Name", "IP", "Target", "Region", "Certificates")

	for _, target := range targetList {
		certsStr := strings.Join(target.Certificates, ",")
		fmt.Fprintf(writer, formatString, target.ForwardingRule, target.IP, target.Target, target.Region, certsStr)
	}
}

var _re_uri, _ = regexp.Compile(`^https?://(www)?\.?\w+\.\w+/[a-zA-Z-/0-9]+`)

// IsURI a regex to check the input whether is uri or not.
func IsURI(input string) bool {
	return _re_uri.MatchString(input)
}

// TransferCertsNamesToURLs transfers the list of certs name to correspond uris
func (g *APIClient) TransferCertsNamesToURLs(certsInput []string) ([]string, error) {
	result := []string{}

	certs, err := g.ListCertificates()
	if err != nil {
		return nil, err
	}

	mapping := CreateCertsMapOfNameToURI(certs)

	for _, input := range certsInput {
		if !IsURI(input) {
			value, ok := mapping[input]
			if !ok {
				return nil, fmt.Errorf("certs: %s not found", input)
			}
			result = append(result, value)
		} else {
			result = append(result, input)
		}
	}

	return result, nil
}

func (g *APIClient) ListCertificates() ([]*compute.SslCertificate, error) {
	// Type: SELF_MANAGED, MANAGED
	// SubjectAlternativeNames to get the domains of this certs

	resp, err := g.computeService.SslCertificates.List(g.project).Context(g.ctx).Do()
	if err != nil {
		return nil, err
	}

	return resp.Items, nil
}

func (g *APIClient) CreateGCPManagedCertificates(certName string, domains []string) (*compute.Operation, error) {
	rb := &compute.SslCertificate{
		Name: certName,
		Type: "MANAGED",
		Managed: &compute.SslCertificateManagedSslCertificate{
			Domains: domains,
		},
	}

	res, err := g.computeService.SslCertificates.Insert(g.project, rb).Context(g.ctx).Do()
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (g *APIClient) GetGCPManagedCertificates(certName string) (*compute.SslCertificate, error) {
	res, err := g.computeService.SslCertificates.Get(g.project, certName).Context(g.ctx).Do()
	return res, err
}

func (g *APIClient) PruneGCPManagedCertificates(certName string, domains ...string) (*compute.SslCertificate, error) {

	return nil, nil
}

func (g *APIClient) MergeGCPManagedCertificates(to string, certNames ...string) (*compute.Operation, error) {
	domains := []string{}

	certs, err := g.ListCertificates()
	if err != nil {
		return nil, err
	}

	for _, name := range certNames {
		for _, cert := range certs {
			if name == cert.Name {
				domains = append(domains, getDomainsOfGCPCerts(cert)...)
			}
		}
	}

	// remove the duplicate domains
	domains = removeDuplicateValue(domains)

	res, err := g.CreateGCPManagedCertificates(to, domains)
	return res, err
}

func removeDuplicateValue(vs []string) []string {
	result := []string{}
	mapping := map[string]string{}

	for _, v := range vs {
		if _, ok := mapping[v]; !ok {
			result = append(result, v)
			mapping[v] = ""
		}
	}

	return result
}

func (g *APIClient) DeleteGCPManagedCertificates(certName string) (*compute.Operation, error) {
	res, err := g.computeService.SslCertificates.Delete(g.project, certName).Context(g.ctx).Do()
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (g *APIClient) pushCertificate(certName, certData, keyData string) (*compute.Operation, error) {

	rb := &compute.SslCertificate{
		Name:        certName,
		Type:        "SELF_MANAGED",
		Certificate: string(certData),
		PrivateKey:  string(keyData),
	}

	res, err := g.computeService.SslCertificates.Insert(g.project, rb).Context(g.ctx).Do()
	if err != nil {
		return nil, err
	}

	return res, nil
}

// pushCertificate(ctx, computeService, project)

// gcloud compute target-https-proxies list to get the forwarding rules

// process is established.
// 1. create a self-managed certificate
// 2. update the target-https-proxy sslcertificates (it seems that need to grab the original cert list first)
//    because it will replace not attach

// TargetHttpsProxiesService) SetSslCertificates(project string, targetHttpsProxy string, targethttpsproxiessetsslcertificatesrequest *TargetHttpsProxiesSetSslCertificatesRequest) *TargetHttpsProxiesSetSslCertificatesCall

type TargetProxyResponse struct {
	ForwardingRule string
	Target         string
	IP             string
	Certificates   []string
	Region         string
}

func extractTheLastPartOfURL(uri string) string {
	tokens := strings.Split(uri, "/")
	return tokens[len(tokens)-1]
}

func transferRegionURIToName(uri string) string {
	return extractTheLastPartOfURL(uri)
}

func transferTargetURIToName(uri string) string {
	return extractTheLastPartOfURL(uri)
}

func transferTargetURIsToNames(uris []string) []string {
	result := []string{}
	for _, uri := range uris {
		result = append(result, transferTargetURIToName(uri))
	}
	return result
}

func (g *APIClient) ListCertOfHTTPSTargetProxy() ([]TargetProxyResponse, error) {

	result := []TargetProxyResponse{}
	targetMapURIToName := map[string]struct {
		Name         string
		Certificates []string
	}{}

	tl, err := g.computeService.TargetHttpsProxies.AggregatedList(g.project).Context(g.ctx).Do()
	if err != nil {
		return nil, err
	}

	for _, item := range tl.Items {

		for _, target := range item.TargetHttpsProxies {
			targetMapURIToName[target.SelfLink] = struct {
				Name         string
				Certificates []string
			}{
				Name:         target.Name,
				Certificates: target.SslCertificates,
			}
		}

	}

	fList, err := g.computeService.ForwardingRules.AggregatedList(g.project).Context(g.ctx).Do()
	if err != nil {
		return nil, err
	}

	for _, forwarding := range fList.Items {
		if len(forwarding.ForwardingRules) > 0 {
			for _, f := range forwarding.ForwardingRules {
				// It seems the region field of global's forwarding rule
				// will be empty string
				if f.Region == "" {
					f.Region = "Global"
				}

				// Currently, we only care about https
				if strings.Contains(f.PortRange, "443") {

					result = append(result, TargetProxyResponse{
						ForwardingRule: f.Name,
						Target:         targetMapURIToName[f.Target].Name,
						Certificates:   transferTargetURIsToNames(targetMapURIToName[f.Target].Certificates),
						IP:             f.IPAddress,
						Region:         transferRegionURIToName(f.Region),
					})
				}

			}
		}
	}

	return result, nil
}

// UpdateCertOfHTTPSTargetProxy update the target proxy's certs
func (g *APIClient) UpdateCertOfHTTPSTargetProxy(targetProxy string, certsName []string) (*compute.Operation, error) {

	certsURIs, err := g.TransferCertsNamesToURLs(certsName)
	if err != nil {
		return nil, err
	}

	certs := &compute.TargetHttpsProxiesSetSslCertificatesRequest{
		SslCertificates: certsURIs,
	}

	res, err := g.computeService.
		TargetHttpsProxies.
		SetSslCertificates(g.project, targetProxy, certs).Context(g.ctx).Do()
	if err != nil {
		return nil, err
	}

	return res, nil
}
