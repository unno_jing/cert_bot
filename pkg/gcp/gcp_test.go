package gcp

import (
	"context"
	"fmt"
	"testing"
)

// go test -v -timeout 30s -run ^TestListCerts$ gop/auto_renew_cert/pkg/gcp
func TestListCerts(t *testing.T) {

	ctx := context.Background()
	// project := "production-a"
	project := "staging-211402"

	client, err := NewAPIClient(ctx, project)
	if err != nil {
		t.Error(err)
	}

	certs, err := client.ListCertificates()
	if err != nil {
		t.Error(err)
	}

	fmt.Println(certs)

}

func TestCreateGCPManagedCertificates(t *testing.T) {

	ctx := context.Background()
	// project := "production-a"
	project := "staging-211402"

	client, err := NewAPIClient(ctx, project)
	if err != nil {
		t.Error(err)
	}

	res, err := client.CreateGCPManagedCertificates("test-from-auto", []string{
		"proxy.h9339.com",
		"staging.h9339.com",
	})

	if err != nil {
		t.Error(err)
	}

	fmt.Println(res)

}

func TestListCertOfHTTPSTargetProxy(t *testing.T) {

	ctx := context.Background()
	project := "staging-211402"

	client, err := NewAPIClient(ctx, project)
	if err != nil {
		t.Error(err)
	}

	items, err := client.ListCertOfHTTPSTargetProxy()

	if err != nil {
		t.Error(err)
	}

	result := CreateForwardingMapOfIPToTarget(items)

	fmt.Println(items)
	fmt.Println(result)
}

func TestUpdateCertOfHTTPTargetProxy(t *testing.T) {

	ctx := context.Background()
	project := "staging-211402"

	client, err := NewAPIClient(ctx, project)
	if err != nil {
		t.Error(err)
	}

	res, err := client.UpdateCertOfHTTPSTargetProxy("lb-caddy-target-proxy-2",
		[]string{
			"https://www.googleapis.com/compute/v1/projects/staging-211402/global/sslCertificates/wizapi2",
			"test-from-auto2",
		})
	if err != nil {
		t.Error(err)
	}

	fmt.Printf("%+v", res)
}
