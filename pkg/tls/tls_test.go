package tls

import (
	"fmt"
	"testing"
)

// go test -v -timeout 30s -run ^TestGetSSLCertificates$ gop/auto_renew_cert/pkg/tls
func TestGetSSLCertificates(t *testing.T) {

	certs, err := GetSSLCertificates("sovip84.com")
	if err != nil {
		t.Error(err)
	}

	fmt.Println(certs[0].DNSNames)
	fmt.Println(certs[0].Issuer)
	fmt.Printf("%+v\n", certs[0])
	fmt.Println(certs[0].NotBefore)
	fmt.Println(certs[0].NotAfter)
}
