package tls

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"strings"
)

func GetSSLCertificates(domain string) ([]*x509.Certificate, error) {
	// "sovip84.com"
	connString := domain + ":443"
	if strings.Contains(domain, ":") {
		domain = strings.Split(domain, ":")[0]
	}

	conn, err := tls.Dial("tcp", connString, nil)
	if err != nil {
		return nil, err
	}

	err = conn.VerifyHostname(domain)
	if err != nil {
		return nil, err
	}

	certs := conn.ConnectionState().PeerCertificates

	return certs, nil
}

func SaveCerts(certs []*x509.Certificate, fname string) error {
	// Let's try save the raw data and see what's that
	if !strings.HasSuffix(fname, ".pem") {
		fname = fname + ".pem"
	}

	if len(certs) > 0 {
		err := ioutil.WriteFile(fname, certs[0].Raw, 0666)
		if err != nil {
			return err
		}
	}

	return nil
}

func readCertPem(file string) ([]byte, error) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// data, err := readCertPem("/Users/jing/Desktop/fullchain.pem")
// if err != nil {
// 	panic(err)
// }

// parsePem(data)

func parsePem(data []byte) *x509.Certificate {
	roots := x509.NewCertPool()

	ok := roots.AppendCertsFromPEM(data)
	if !ok {
		panic("failed to parse certificate PEM")
	}

	block, _ := pem.Decode(data)
	if block == nil {
		panic("failed to decode certificate PEM")
	}

	fmt.Printf("%+v\n", block)

	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		panic(err)
	}

	// the following two are range of the expiry date
	fmt.Println(cert.NotBefore)
	fmt.Println(cert.NotAfter)
	return cert
}
