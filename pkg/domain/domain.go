package domain

import (
	"io/ioutil"
	"net"
	"regexp"
	"time"
)

// whois protocol https://tools.ietf.org/html/rfc3912

var (
	// TODO: find out where to get this server position
	IANAWhoisServer = "whois.iana.org"
	// IANAWhoisServer = "whois.ename.com"

	WhoisPORT             = "43"
	_referURL, _          = regexp.Compile(`refer:\s+(.+)`)
	_registrarWhoIsURL, _ = regexp.Compile(`Registrar WHOIS Server:\s+([0-9a-zA-Z.-]+)`)
)

func Whois(domain string) (string, error) {
	result, err := whois(domain, IANAWhoisServer)
	if err != nil {
		return "", err
	}

	for _, pattern := range []*regexp.Regexp{_referURL, _registrarWhoIsURL} {
		if pattern.MatchString(result) {
			whoisServer := pattern.FindStringSubmatch(result)[1]
			temp, err := whois(domain, whoisServer)
			if err != nil {
				return "", err
			}
			// result = result + "\n# " + whoisServer + "\n"
			result = temp
		}

	}

	return result, nil
}

func whois(domain string, server string) (string, error) {

	conn, err := net.DialTimeout("tcp", net.JoinHostPort(server, WhoisPORT), time.Second*30)
	if err != nil {
		return "", err
	}
	defer conn.Close()

	_ = conn.SetWriteDeadline(time.Now().Add(time.Second * 30))
	_, err = conn.Write([]byte(domain + "\r\n"))
	if err != nil {
		return "", err
	}

	_ = conn.SetReadDeadline(time.Now().Add(time.Second * 30))
	result, err := ioutil.ReadAll(conn)
	if err != nil {
		return "", err
	}

	return string(result), nil
}
