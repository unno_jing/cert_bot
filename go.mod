module bitbucket.org/unno_jing/cert_bot

go 1.13

require (
	github.com/magefile/mage v1.13.0 // indirect
	github.com/urfave/cli/v2 v2.3.0
	google.golang.org/api v0.31.0
	gopkg.in/square/go-jose.v2 v2.5.1
	gopkg.in/yaml.v2 v2.2.8
)
