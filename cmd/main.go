package main

import (
	"fmt"
	"os"

	handler "bitbucket.org/unno_jing/cert_bot"
)

func main() {
	args := os.Args
	fmt.Println(handler.PerformCommands(args))
}
