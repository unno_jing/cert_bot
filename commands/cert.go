package commands

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/unno_jing/cert_bot/pkg/gcp"

	"github.com/urfave/cli/v2"
)

// RegisterGCPCertCmd define the root of the gcp cert commands
func RegisterGCPCertCmd() *cli.Command {
	return &cli.Command{
		Name:        "cert",
		Description: "google certs management.",
		Usage:       "options for certificate",
	}
}

func RegisterGCPCertCreateCmd() *cli.Command {

	return &cli.Command{
		Name:    "create",
		Aliases: []string{"c"},
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "name", Aliases: []string{"n"}},
			&cli.StringFlag{Name: "project", Aliases: []string{"p"}},
			&cli.StringFlag{Name: "domains", Aliases: []string{"d"}},
		},
		Usage: "create a new certificates",
		Action: func(c *cli.Context) error {

			// go run main.go cert create -p staging-211402 -n test-from-auto2 -d awx.h9339.com,staging.h9339.com
			project := c.String("project")
			certName := c.String("name")
			domains := strings.Split(c.String("domains"), ",")
			ctx := context.Background()

			client, err := gcp.NewAPIClient(ctx, project)
			if err != nil {
				return err
			}

			res, err := client.CreateGCPManagedCertificates(certName, domains)
			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "certs %s is created\n", res.Name)
			return nil
		},
	}
}

func RegisterGCPCertDeletecmd() *cli.Command {
	return &cli.Command{
		Name:    "delete",
		Aliases: []string{"d"},
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "name", Aliases: []string{"n"}},
			&cli.StringFlag{Name: "project", Aliases: []string{"p"}},
		},
		Usage: "delete a certitifcate",
		Action: func(c *cli.Context) error {

			project := c.String("project")
			certName := c.String("name")
			ctx := context.Background()

			client, err := gcp.NewAPIClient(ctx, project)
			if err != nil {
				return err
			}

			res, err := client.DeleteGCPManagedCertificates(certName)
			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "delete response code: %d\n", res.ServerResponse.HTTPStatusCode)
			return nil
		},
	}
}

func RegisterGCPCertPrunecmd() *cli.Command {
	return &cli.Command{
		Name:    "prune",
		Aliases: []string{"p"},
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "name", Aliases: []string{"n"}},
			&cli.StringFlag{Name: "project", Aliases: []string{"p"}},
		},
		Usage: "prune a certificate",
		Action: func(c *cli.Context) error {
			project := c.String("project")
			certName := c.String("name")
			ctx := context.Background()

			client, err := gcp.NewAPIClient(ctx, project)
			if err != nil {
				return err
			}

			res, err := client.PruneGCPManagedCertificates(certName)
			if err != nil {
				return err
			}

			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "prune response code: %d\n", res.ServerResponse.HTTPStatusCode)
			return nil

		},
	}
}

func RegisterGCPCertMergecmd() *cli.Command {

	return &cli.Command{
		Name:    "merge",
		Aliases: []string{"m"},
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "project", Aliases: []string{"p"}},
			&cli.StringFlag{Name: "to", Aliases: []string{"t"}},
		},
		Usage:     "create a new certs from the given name of existing certs.",
		ArgsUsage: "array of names [options]",
		Action: func(c *cli.Context) error {

			project := c.String("project")
			names := strings.Split(c.Args().Get(0), ",")
			to := c.String("to")
			ctx := context.Background()

			client, err := gcp.NewAPIClient(ctx, project)
			if err != nil {
				return err
			}

			_, err = client.MergeGCPManagedCertificates(to, names...)
			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "merge certs to a new one (%s)\n", to)

			return nil
		},
	}
}

func RegisterGCPCertListcmd() *cli.Command {
	return &cli.Command{
		Name:    "list",
		Aliases: []string{"l"},
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "project", Aliases: []string{"p"}},
		},
		Usage: "list certificates",
		Action: func(c *cli.Context) error {
			project := c.String("project")
			ctx := context.Background()
			client, err := gcp.NewAPIClient(ctx, project)
			if err != nil {
				return err
			}

			certs, err := client.ListCertificates()
			if err != nil {
				return err
			}

			gcp.PrettyPrintCerts(c.App.Writer, certs)

			return nil
		},
	}
}
