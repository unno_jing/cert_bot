package commands

import (
	"fmt"

	"bitbucket.org/unno_jing/cert_bot/pkg/domain"

	"github.com/urfave/cli/v2"
)

func RegisterWhoisCmd() *cli.Command {
	return &cli.Command{
		Name:      "whois",
		Usage:     "options for whois",
		ArgsUsage: "[domain]",
		Action: func(c *cli.Context) error {
			uri := c.Args().Get(0)
			result, err := domain.Whois(uri)
			if err != nil {
				return err
			}

			fmt.Fprint(c.App.Writer, result)
			return nil
		},
	}
}
