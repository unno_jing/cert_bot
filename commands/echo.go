package commands

import (
	"fmt"
	"strings"

	"github.com/urfave/cli/v2"
)

func RegisterEchoCmd() *cli.Command {
	return &cli.Command{
		Name:  "echo",
		Usage: "options for echo",
		Action: func(c *cli.Context) error {
			result := strings.Join(c.Args().Slice(), " ")
			fmt.Fprint(c.App.Writer, result)
			return nil
		},
	}
}
