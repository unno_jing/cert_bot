package commands

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/unno_jing/cert_bot/pkg/gcp"

	"github.com/urfave/cli/v2"
)

// RegisterGCPHttpTargetProxy define the root of the gcp http target proxy commands
func RegisterGCPHttpTargetProxy() *cli.Command {
	return &cli.Command{
		Name:  "httpstargetproxy",
		Usage: "google https target proxy management.",
	}
}

func RegisterGCPHttpTargetProxyListCmd() *cli.Command {

	return &cli.Command{
		Name:    "list",
		Aliases: []string{"l"},
		Usage:   "list https target proxy",
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "project", Aliases: []string{"p"}},
		},
		Action: func(c *cli.Context) error {
			project := c.String("project")
			ctx := context.Background()
			client, err := gcp.NewAPIClient(ctx, project)
			if err != nil {
				return err
			}

			list, err := client.ListCertOfHTTPSTargetProxy()
			if err != nil {
				return err
			}

			gcp.PrettyPrintTargetProxy(c.App.Writer, list)
			return nil
		},
	}
}

func RegisterGCPHttpTargetProxyAppendCertCmd() *cli.Command {
	return &cli.Command{
		Name:    "append_cert",
		Aliases: []string{"a"},
		Usage:   "append certs to https target proxy's certs list instead of repace it.",
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "name", Aliases: []string{"n"}},
			&cli.StringFlag{Name: "ip", Aliases: []string{"i"}},
			&cli.StringFlag{Name: "certnames", Aliases: []string{"c"}},
			&cli.StringFlag{Name: "project", Aliases: []string{"p"}},
		},
		Action: func(c *cli.Context) error {
			targetProxyName := c.String("name")
			certNames := []string{}
			for _, s := range strings.Split(c.String("certnames"), ",") {
				certNames = append(certNames, strings.TrimSpace(s))
			}

			project := c.String("project")
			ctx := context.Background()
			client, err := gcp.NewAPIClient(ctx, project)
			if err != nil {
				return err
			}

			targetRes, err := client.ListCertOfHTTPSTargetProxy()
			if err != nil {
				return err
			}
			mapping := gcp.CreateForwardingMapOfIPToTarget(targetRes)

			if c.String("ip") != "" {
				targetProxyName = mapping[c.String("ip")]
			}

			for _, p := range targetRes {
				if p.Target == targetProxyName {
					certNames = append(certNames, p.Certificates...)
					break
				}
			}

			res, err := client.UpdateCertOfHTTPSTargetProxy(targetProxyName, certNames)
			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "update response code: %d\n", res.ServerResponse.HTTPStatusCode)
			return nil
		},
	}

}

func RegisterGCPHttpTargetProxyUpdateCertCmd() *cli.Command {
	return &cli.Command{
		Name:    "update",
		Aliases: []string{"u"},
		Usage:   "udpate https target proxy's certs",
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "name", Aliases: []string{"n"}},
			&cli.StringFlag{Name: "ip", Aliases: []string{"i"}},
			&cli.StringFlag{Name: "certnames", Aliases: []string{"c"}},
			&cli.StringFlag{Name: "project", Aliases: []string{"p"}},
		},
		Action: func(c *cli.Context) error {
			// go run main.go httpstargetproxy update -p staging-211402 -n lb-caddy-target-proxy-2 -c wizapi2,test-from-auto2
			targetProxyName := c.String("name")
			certNames := []string{}
			for _, s := range strings.Split(c.String("certnames"), ",") {
				certNames = append(certNames, strings.TrimSpace(s))
			}

			project := c.String("project")
			ctx := context.Background()
			client, err := gcp.NewAPIClient(ctx, project)
			if err != nil {
				return err
			}

			targetRes, err := client.ListCertOfHTTPSTargetProxy()
			if err != nil {
				return err
			}
			mapping := gcp.CreateForwardingMapOfIPToTarget(targetRes)

			if c.String("ip") != "" {
				targetProxyName = mapping[c.String("ip")]
			}

			res, err := client.UpdateCertOfHTTPSTargetProxy(targetProxyName, certNames)
			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "update response code: %d\n", res.ServerResponse.HTTPStatusCode)
			return nil
		},
	}
}

func RegisterGCPHttpTargetProxyRemoveCertCmd() *cli.Command {
	// example usage.
	// httpstargetproxy remove_cert -c okla -i 34.107.143.218 -p staging-211402
	return &cli.Command{
		Name:    "remove_cert",
		Aliases: []string{"r"},
		Usage:   "remove certs from the https target proxy's cert list",
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "name", Aliases: []string{"n"}},
			&cli.StringFlag{Name: "ip", Aliases: []string{"i"}},
			&cli.StringFlag{Name: "certnames", Aliases: []string{"c"}},
			&cli.StringFlag{Name: "project", Aliases: []string{"p"}},
		},
		Action: func(c *cli.Context) error {
			targetProxyName := c.String("name")
			certNamesToBeRemoved := map[string]struct{}{}
			for _, s := range strings.Split(c.String("certnames"), ",") {
				certNamesToBeRemoved[strings.TrimSpace(s)] = struct{}{}
				// the certs wil be removed
			}

			project := c.String("project")
			ctx := context.Background()
			client, err := gcp.NewAPIClient(ctx, project)
			if err != nil {
				return err
			}

			targetRes, err := client.ListCertOfHTTPSTargetProxy()
			if err != nil {
				return err
			}

			mapping := gcp.CreateForwardingMapOfIPToTarget(targetRes)

			if c.String("ip") != "" {
				targetProxyName = mapping[c.String("ip")]
			}

			newCerts := []string{}
			// remove the certs from the target proxy
			for _, p := range targetRes {
				if p.Target == targetProxyName {
					for _, cert := range p.Certificates {
						if _, ok := certNamesToBeRemoved[cert]; !ok {
							newCerts = append(newCerts, cert)
						}
					}
					break
				}
			}

			res, err := client.UpdateCertOfHTTPSTargetProxy(targetProxyName, newCerts)
			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "update response code: %d\n", res.ServerResponse.HTTPStatusCode)
			return nil

		},
	}
}
