package commands

import (
	"fmt"

	"bitbucket.org/unno_jing/cert_bot/pkg/tls"

	"github.com/urfave/cli/v2"
)

// RegisterSSL define the root of the ssl commands
func RegisterSSL() *cli.Command {
	return &cli.Command{
		Name:  "ssl",
		Usage: "options for ssl",
	}
}

func RegisterSSLCheckCmd() *cli.Command {
	return &cli.Command{
		Name:      "check",
		Aliases:   []string{"c"},
		Usage:     "validate domain's ssl",
		ArgsUsage: "[domain]",
		Action: func(c *cli.Context) error {
			domain := c.Args().Get(0)
			certs, err := tls.GetSSLCertificates(domain)
			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "Domains: %+v\n", certs[0].DNSNames)
			fmt.Fprintf(c.App.Writer, "Issuer: %s\n", certs[0].Issuer)
			// fmt.Printf("%+v\n", certs[0])
			fmt.Fprintf(c.App.Writer, "Validated date range: %s - %s\n", certs[0].NotBefore, certs[0].NotAfter)

			return nil
		},
	}

}
