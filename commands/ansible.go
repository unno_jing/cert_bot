package commands

import (
	"context"
	"fmt"
	"net"
	"strings"

	"bitbucket.org/unno_jing/cert_bot/pkg/ansible"
	"bitbucket.org/unno_jing/cert_bot/pkg/gcp"
	"github.com/urfave/cli/v2"
	"gopkg.in/yaml.v2"
)

func RegisterAnsibleCmd() *cli.Command {
	return &cli.Command{
		Name:        "ansible",
		Description: "ansible tower management",
		Usage:       "options for ansible tower",
	}
}

func addIsMaintained(target string, t *ansible.TriggerJobPayLoad) (string, error) {

	data := map[string]interface{}{}
	err := yaml.Unmarshal([]byte(t.ExtraVars), &data)
	if err != nil {
		return "", err
	}

	for _, obj := range data["nginx_confs"].([]interface{}) {

		o := obj.(map[interface{}]interface{})

		if strings.ToLower(o["name"].(string)) == target {
			o["is_maintained"] = true
		}
	}

	content, err := yaml.Marshal(data)
	if err != nil {
		return "", err
	}

	extraVars := "---\n" + string(content)

	return extraVars, nil
}

func RegisterAnsibleMaintainFrontendCmd() *cli.Command {
	return &cli.Command{
		Name:    "maintain",
		Aliases: []string{"m"},
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "target", Aliases: []string{"t"}},
		},
		Usage: "maintain the frontend of certain branch",
		Action: func(c *cli.Context) error {

			name := c.String("name")
			if name == "" {
				name = "deploy staging frontend"
			}

			res, err := ansible.TriggerJobTemplate(name, func(t *ansible.TriggerJobPayLoad) error {
				// process the extra_vars arguments.
				target := strings.ToLower(c.String("target"))
				data, err := addIsMaintained(target, t)
				if err != nil {
					return err
				}

				// prepare the extra of the domains need to be maintained
				t.ExtraVars = data

				return nil
			})

			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "trigger job: %d, %s. to maintain the target website. go https://staging-awx.h9339.com%s to see more detail", res.ID, res.Name, res.URL)
			return nil
		},
	}
}

func RegisterAnsibleUnMaintainFrontendCmd() *cli.Command {
	return &cli.Command{
		Name:    "unmaintain",
		Aliases: []string{"u"},
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "name", Aliases: []string{"n"}},
			&cli.StringFlag{Name: "target", Aliases: []string{"t"}},
		},
		Usage: "remove maintaining the frontend of certain branch",
		Action: func(c *cli.Context) error {

			name := c.String("name")
			if name == "" {
				name = "deploy staging frontend"
			}

			res, err := ansible.TriggerJobTemplate(name, func(t *ansible.TriggerJobPayLoad) error {
				// we can do redeploy the job and do nothing in this processor.
				return nil
			})

			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "trigger job: %d, %s. to remove maintaining the frontend. go https://staging-awx.h9339.com%s to see more detail", res.ID, res.Name, res.URL)
			return nil

		},
	}
}

func removeDuplicateValue(vs []interface{}) []interface{} {
	result := []interface{}{}
	mapping := map[interface{}]struct{}{}

	for _, v := range vs {
		if _, ok := mapping[v]; !ok {
			result = append(result, v)
			mapping[v] = struct{}{}
		}
	}

	return result
}

func appendDomainsToJobTmpl(target string, domains []string, t *ansible.TriggerJobPayLoad) (string, error) {

	ds := []interface{}{}

	for _, d := range domains {
		ds = append(ds, d)
	}

	data := map[string]interface{}{}
	err := yaml.Unmarshal([]byte(t.ExtraVars), &data)
	if err != nil {
		return "", err
	}

	for _, obj := range data["nginx_confs"].([]interface{}) {

		o := obj.(map[interface{}]interface{})

		if strings.ToLower(o["name"].(string)) == target {
			server_name := o["server_name"].([]interface{})
			server_name = append(server_name, ds...)
			server_name = removeDuplicateValue(server_name)
			o["server_name"] = server_name
			break
		}
	}

	content, err := yaml.Marshal(data)
	if err != nil {
		return "", err
	}

	extraVars := "---\n" + string(content)
	return extraVars, nil
}

func getFirstDomainOfTargetInAnsbileTmpl(target string, t *ansible.TriggerJobPayLoad) (string, error) {

	data := map[string]interface{}{}
	err := yaml.Unmarshal([]byte(t.ExtraVars), &data)
	if err != nil {
		return "", err
	}

	for _, obj := range data["nginx_confs"].([]interface{}) {

		o := obj.(map[interface{}]interface{})

		if strings.ToLower(o["name"].(string)) == target {
			server_name := o["server_name"].([]interface{})
			return server_name[0].(string), nil
		}
	}

	return "", fmt.Errorf("can not find any in domain in the %s", target)
}

func lookupLoadBalancerByReverseDomain(url string, client *gcp.APIClient) (string, error) {
	ips, err := net.LookupIP(url)
	if err != nil {
		return "", err
	}

	ip := ips[0].String()

	targetRes, err := client.ListCertOfHTTPSTargetProxy()
	if err != nil {
		return "", err
	}
	mapping := gcp.CreateForwardingMapOfIPToTarget(targetRes)

	return mapping[ip], nil
}

func RegisterAnsibleAddDomainsAndDeploy() *cli.Command {
	// example usage
	// go run main.go ansible addnewdomains -n "deploy staging frontend" -t testadmin.conf -d "admin1.h9339.com,admin2.h9339.com" --withcerts --certname "okla" -p "staging-211402"

	// 1. Edit job
	// 2. launch, may be failed how to resolve
	// 3. create certs and attach to load balancer
	// carefully handle the error processing
	return &cli.Command{
		Name: "addnewdomains",
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "name", Aliases: []string{"n"}},
			&cli.StringFlag{Name: "domains", Aliases: []string{"d"}},
			&cli.StringFlag{Name: "target", Aliases: []string{"t"}, Usage: "the section in the job template's yaml"},
			&cli.BoolFlag{Name: "withcerts"},
			&cli.StringFlag{Name: "certname"},
			&cli.StringFlag{Name: "project", Aliases: []string{"p"}},
		},
		Usage: "add new domains for frontend service",
		Action: func(c *cli.Context) error {
			name := c.String("name")
			if name == "" {
				name = "deploy staging frontend"
			}

			var client *gcp.APIClient
			var err error
			withcerts := c.Bool("withcerts")
			domains := strings.Split(c.String("domains"), ",")
			targetProxyName := ""

			if withcerts {
				project := c.String("project")
				ctx := context.Background()
				client, err = gcp.NewAPIClient(ctx, project)
				if err != nil {
					return err
				}
			}

			res, err := ansible.EditJobTemplate(name, func(t *ansible.TriggerJobPayLoad) error {

				target := strings.ToLower(c.String("target"))
				data, err := appendDomainsToJobTmpl(target, domains, t)
				if err != nil {
					return err
				}

				t.ExtraVars = data
				return nil
			})

			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "edit job: %d, %s completed\n", res.ID, res.Name)

			res, err = ansible.TriggerJobTemplate(name, func(t *ansible.TriggerJobPayLoad) error {
				// grab the first ip of the target, this will be used for attaching
				// certs to the target load balancer
				if withcerts {

					target := strings.ToLower(c.String("target"))
					url, err := getFirstDomainOfTargetInAnsbileTmpl(target, t)
					if err != nil {
						return err
					}

					targetProxyName, err = lookupLoadBalancerByReverseDomain(url, client)
					if err != nil {
						return err
					}
				}
				return nil
			})

			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "trigger deploy job: %d, %s\n", res.ID, res.Name)

			// start to create certs and attach to the target load balancer
			if withcerts {
				certName := c.String("certname")
				if certName == "" {
					return fmt.Errorf("certname is not provided.")
				}

				res, err := client.CreateGCPManagedCertificates(certName, domains)
				if err != nil {
					return err
				}

				fmt.Fprintf(c.App.Writer, "certs %s is created\n", res.Name)

				certNames := []string{certName}
				targetRes, err := client.ListCertOfHTTPSTargetProxy()
				if err != nil {
					return err
				}

				for _, p := range targetRes {
					if p.Target == targetProxyName {
						certNames = append(certNames, p.Certificates...)
						break
					}
				}

				res, err = client.UpdateCertOfHTTPSTargetProxy(targetProxyName, certNames)
				if err != nil {
					return err
				}
				fmt.Fprintf(c.App.Writer, "update response code: %d\n", res.ServerResponse.HTTPStatusCode)
			}

			return nil
		},
	}
}

func RegisterAnsibleTriggerCmd() *cli.Command {
	// ex. ansible trigger -n "install nginx" -i "gcp staging frontend"
	return &cli.Command{
		Name:    "trigger",
		Aliases: []string{"t"},
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "name", Aliases: []string{"n"}},
			&cli.StringFlag{Name: "inventory", Aliases: []string{"i"}},
		},
		Usage: "trigger ansible job",
		Action: func(c *cli.Context) error {

			inventoryID := -1
			var err error

			name := c.String("name")

			if c.String("inventory") != "" {
				inventoryID, err = ansible.GetInventoryIDByName(c.String("inventory"))
				if err != nil {
					return err
				}
			}

			res, err := ansible.TriggerJobTemplate(name, func(t *ansible.TriggerJobPayLoad) error {
				// need to payload inventory
				if inventoryID != -1 {
					t.Inventory = inventoryID
				}

				return nil
			})

			if err != nil {
				return err
			}

			fmt.Fprintf(c.App.Writer, "trigger job: %d, %s. go https://staging-awx.h9339.com%s to see more detail", res.ID, res.Name, res.URL)
			return nil
		},
	}
}
