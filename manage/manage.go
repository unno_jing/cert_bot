package manage

import (
	"github.com/urfave/cli/v2"
)

func RegisterCommand(app *cli.App, commander func() *cli.Command) *cli.Command {
	cmd := commander()
	app.Commands = append(app.Commands, cmd)
	return cmd
}

func RegisterSubCommand(app *cli.App, parent *cli.Command, commander func() *cli.Command) {
	subcmds := commander()
	parent.Subcommands = append(parent.Subcommands, subcmds)
}
