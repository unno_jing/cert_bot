package handler

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestParseMetrics(t *testing.T) {

	content := "[ var='B0' metric='ghost-78567ffd56-7ps7b' labels={pod=ghost-78567ffd56-7ps7b} value=0.27457304663672766 ], [ var='B1' metric='ghost-78567ffd56-8pzgq' labels={pod=ghost-78567ffd56-8pzgq} value=0.25986841973506186 ], [ var='B2' metric='ghost-78567ffd56-gkwb6' labels={pod=ghost-78567ffd56-gkwb6} value=0.23018106843145453 ], [ var='B3' metric='ghost-78567ffd56-nfjp4' labels={pod=ghost-78567ffd56-nfjp4} value=0.2635864958046286 ], [ var='B4' metric='ghost-78567ffd56-tb72n' labels={pod=ghost-78567ffd56-tb72n} value=0.29957900127242937 ], [ var='B5' metric='ghost-78567ffd56-tmtw8' labels={pod=ghost-78567ffd56-tmtw8} value=0.3169962672428377 ]"

	result := parseMetrics(content)
	fmt.Println(result)

}

func TestUnmarshalGrafanaWebhook(t *testing.T) {

	content := []byte(`
{
  "receiver": "my-telegram",
  "status": "firing",
  "alerts": [
    {
      "status": "firing",
      "labels": {
        "alertname": "CPU-api",
        "notification": "telegram"
      },
      "annotations": {
        "summary": "api cpu 需要注意"
      },
      "startsAt": "2022-08-23T10:08:07.707257776Z",
      "endsAt": "0001-01-01T00:00:00Z",
      "generatorURL": "https://monitor.grafana.clzud.live/alerting/grafana/tq590CiVz/view",
      "fingerprint": "b4572834453a58f0",
      "silenceURL": "https://monitor.grafana.clzud.live/alerting/silence/new?alertmanager=grafana&matcher=alertname%3DCPU-api&matcher=notification%3Dtelegram",
      "dashboardURL": "https://monitor.grafana.clzud.live/d/kcTW2jm4k",
      "panelURL": "https://monitor.grafana.clzud.live/d/kcTW2jm4k?viewPanel=9",
      "valueString": "[ var='B0' metric='ghost-78567ffd56-7ps7b' labels={pod=ghost-78567ffd56-7ps7b} value=0.27457304663672766 ], [ var='B1' metric='ghost-78567ffd56-8pzgq' labels={pod=ghost-78567ffd56-8pzgq} value=0.25986841973506186 ], [ var='B2' metric='ghost-78567ffd56-gkwb6' labels={pod=ghost-78567ffd56-gkwb6} value=0.23018106843145453 ], [ var='B3' metric='ghost-78567ffd56-nfjp4' labels={pod=ghost-78567ffd56-nfjp4} value=0.2635864958046286 ], [ var='B4' metric='ghost-78567ffd56-tb72n' labels={pod=ghost-78567ffd56-tb72n} value=0.29957900127242937 ], [ var='B5' metric='ghost-78567ffd56-tmtw8' labels={pod=ghost-78567ffd56-tmtw8} value=0.3169962672428377 ]",
      "imageURL": "https://monitor.grafana.clzud.live/public/img/attachments/r6Hl5tCdYP5jIoAKf99V.png"
    }
  ],
  "groupLabels": {},
  "commonLabels": {
    "alertname": "CPU-api",
    "notification": "telegram"
  },
  "commonAnnotations": {
    "summary": "api cpu 需要注意"
  },
  "externalURL": "https://monitor.grafana.clzud.live/",
  "version": "1",
  "groupKey": "{}/{notification=\"telegram\"}:{}",
  "truncatedAlerts": 0,
  "orgId": 1,
  "title": "[FIRING:1] (CPU-api telegram)",
  "state": "alerting",
  "message": "**Firing**\n\nValue: [ var='B0' metric='ghost-78567ffd56-7ps7b' labels={pod=ghost-78567ffd56-7ps7b} value=0.27457304663672766 ], [ var='B1' metric='ghost-78567ffd56-8pzgq' labels={pod=ghost-78567ffd56-8pzgq} value=0.25986841973506186 ], [ var='B2' metric='ghost-78567ffd56-gkwb6' labels={pod=ghost-78567ffd56-gkwb6} value=0.23018106843145453 ], [ var='B3' metric='ghost-78567ffd56-nfjp4' labels={pod=ghost-78567ffd56-nfjp4} value=0.2635864958046286 ], [ var='B4' metric='ghost-78567ffd56-tb72n' labels={pod=ghost-78567ffd56-tb72n} value=0.29957900127242937 ], [ var='B5' metric='ghost-78567ffd56-tmtw8' labels={pod=ghost-78567ffd56-tmtw8} value=0.3169962672428377 ]\nLabels:\n - alertname = CPU-api\n - notification = telegram\nAnnotations:\n - summary = api cpu 需要注意\nSource: https://monitor.grafana.clzud.live/alerting/grafana/tq590CiVz/view\nSilence: https://monitor.grafana.clzud.live/alerting/silence/new?alertmanager=grafana&matcher=alertname%3DCPU-api&matcher=notification%3Dtelegram\nDashboard: https://monitor.grafana.clzud.live/d/kcTW2jm4k\nPanel: https://monitor.grafana.clzud.live/d/kcTW2jm4k?viewPanel=9\n"
}`)

	var data GrafanaWebhook
	if err := json.Unmarshal(content, &data); err != nil {
		t.Error(err)
	}

	fmt.Printf("%+v\n", data)

}

func TestParseCommandMessage(t *testing.T) {
	commands := parseCommandMessage(`/bot ansible addnewdomains -n "deploy staging frontend" -t testadmin.conf -d "admin1.h9339.com,admin2.h9339.com" --withcerts --certname okla -p staging-211402`)

	result := PerformCommands(commands)
	print(result)
}

// func TestSendPhtoto(t *testing.T) {
// 	chatID := -533036721
// 	res, err := sendPhoto(chatID, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZIAwBWKV1O-2kTxJJRIve6BLmMUN1--MV_A&usqp=CAU")
// 	if err != nil {
// 		t.Error(err)
// 	}

// 	hookContent := &GrafanaWebhook{
// 		Title:    "鴉洲桶神",
// 		State:    "alerting",
// 		RuleURL:  "https://monitor.grafana.h9339.com/",
// 		RuleName: "睡公園",
// 		Message:  "啊啊 伊代伊代",
// 		EvalMatches: []EvalMatch{
// 			{
// 				Metric: "體脂",
// 				Value:  20,
// 			},
// 		},
// 	}
// 	result, err := generateAlertMessage(hookContent)
// 	if err != nil {
// 		t.Error(err)
// 	}

// 	res, err = sendMsg(chatID, result)
// 	if err != nil {
// 		t.Error(err)
// 	}

// 	fmt.Println(res)
// }
